#!/usr/bin/env bash

echo "first do this and go to the folder: git clone https://github.com/flathub/org.kde.kate"
flatpak install org.kde.Sdk
git pull
flatpak-builder build-dir org.kde.kate.json
flatpak-builder --user --install --force-clean build-dir org.kde.kate.json
echo "Now you can flatpak run org.kde.kate  "