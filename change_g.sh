#!/usr/bin/env bash

# Change G glyph for IBM Plex Sans font
for f in ./IBMPlexSans-*.otf
    do pyftfeatfreeze -f 'ss02,ss04' $f ./changed_g/$f
done
