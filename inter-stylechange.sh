#!/usr/bin/env bash
#copy this file to the OpenType folder. It will override the fonts in the folder.
echo "Setting up venv"
python -m venv venv
source ./venv/bin/activate
pip install opentype-feature-freezer

echo "Disambiguation ss02"
for f in ./Inter*.otf
    do pyftfeatfreeze -f 'ss02' $f ./$f
done

echo "All done!"