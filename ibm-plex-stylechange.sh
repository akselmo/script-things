#!/usr/bin/env bash
#copy this file to the OpenType folder. It will override the fonts in the folder.
echo "Setting up venv"
python -m venv venv
source ./venv/bin/activate
pip install opentype-feature-freezer

echo "Sans: Changing g glyph, adding dotted zero"
for f in ./IBM-Plex-Sans/IBMPlexSans-*.otf
    do pyftfeatfreeze -f 'ss02,ss04' $f ./$f
done

echo "Mono: Changing g glyph"
for f in ./IBM-Plex-Mono/IBMPlexMono-*.otf
    do pyftfeatfreeze -f 'ss02' $f ./$f
done

echo "Serif: Changing g glyph, adding dotted zero"
for f in ./IBM-Plex-Serif/IBMPlexSerif-*.otf
    do pyftfeatfreeze -f 'ss02,ss04' $f ./$f
done

echo "All done!"